import { HostListener } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MenuItem } from '../menu-item';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuItems: MenuItem[] = [
    {
      label: 'Sign Up',
      icon: 'login'
    },
    {
      label: 'About',
      icon: 'help'
    },
    {
      label: 'Pricing',
      icon: 'attach_money'
    },
    {
      label: 'Docs',
      icon: 'notes'
    },
    {
      label: 'Showcase',
      icon: 'slideshow'
    },
    {
      label: 'Blog',
      icon: 'rss_feed'
      
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

  header_variable = false;
  @HostListener("document:scroll")
  scrollfunction(){
    if(document.body.scrollTop > 0 || document.documentElement.scrollTop > 0){
      this.header_variable = true;
    } 
    else{
      this.header_variable = false;
    }
  }

  toHome(){
    document.getElementById("home").scrollIntoView({behavior:"smooth"});
  }
  toSpecs(){
    document.getElementById("specs").scrollIntoView({behavior:"smooth"});
  }

  toAbout(){
    document.getElementById("about").scrollIntoView({behavior:"smooth"});
  }

  toDrive(){
    document.getElementById("drive").scrollIntoView({behavior:"smooth"});
  }

}
