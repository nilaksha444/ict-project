import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VegaDriveComponent } from './vega-drive.component';

describe('VegaDriveComponent', () => {
  let component: VegaDriveComponent;
  let fixture: ComponentFixture<VegaDriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VegaDriveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VegaDriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
