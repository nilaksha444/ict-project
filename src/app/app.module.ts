import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';



import 'hammerjs';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { FooterComponent } from './static-page-component/footer/footer.component';
import { SpecsComponent } from './static-page-component/specs/specs.component';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { SignupComponent } from './signup/signup.component';
import { AboutusComponent } from './static-page-component/aboutus/aboutus.component';
import { VegaDriveComponent } from './static-page-component/vega-drive/vega-drive.component';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import { TestdashboardComponent } from './testdashboard/testdashboard.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SpecsComponent,
    LandingPageComponent,
    FooterComponent,
    TopmenuComponent,
    SignupComponent,
    AboutusComponent,
    VegaDriveComponent,
    VehicleDetailsComponent,
    TestdashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
