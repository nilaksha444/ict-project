export class carDetails{
    carname: string;
    power: number;
    acceleration: string;
    topspeed: number;
    price: number;
}